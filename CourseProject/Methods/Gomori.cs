﻿using CourseProject.Models;
using System;
using System.Linq;

namespace CourseProject.Methods
{
    internal class Gomori
    {
        public TrainStation trainStation;

        public int fastTrains = 0;
        public int passTrains = 0;

        /// <summary>
        /// Функція приймає на вхід матрицю коефіцієнтів A, вектор b, вектор c і індекси базисних змінних B.
        /// </summary>
        public static double[] SolveWithGomoriMethod(double[,] A, double[] b, double[] c, int[] B)
        {
            int m = b.Length; // кількість обмежень
            int n = c.Length; // кількість змінних

            // Додаємо штучних змінних
            int s = m;
            double[,] newA = new double[m, n + m];
            double[] newc = new double[n + m];
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                    newA[i, j] = A[i, j];

                newA[i, n + i] = 1;
                newc[n + i] = -1;
            }

            // Перевіряємо, чи всі b >= 0
            for (int i = 0; i < m; i++)
            {
                if (b[i] < 0)
                {
                    for (int j = 0; j < n + m; j++)
                        newA[i, j] *= -1;

                    b[i] *= -1;
                }
            }

            // Знаходимо початковий базис
            int[] B0 = new int[m];
            for (int i = 0; i < m; i++)
                B0[i] = n + i;

            // Знаходимо опорний вектор
            double[] x0 = new double[n + m];
            double[] y = new double[m];
            double v = 0;
            for (int i = 0; i < m; i++)
                y[i] = b[i];

            // Розв'язуємо задачу оптимізації за допомогою симплекс-методу
            Tuple<double, double[]> simplexResult = new Simplex(c, A, b).maximize();
            x0 = simplexResult.Item2;
            v = simplexResult.Item1;

            // Якщо значення цільової функції від'ємне, то задача нерозв'язна
            if (v < 0)
                throw new Exception("The problem is infeasible.");

            // Поки є штучні змінні в базисі
            while (B0.Any(lmd => lmd >= n))
            {
                int e = Array.FindIndex(B0, lmd2 => lmd2 >= n); // індекс штучної змінної в базисі
                int j = -1;
                for (int i = 0; i < n; i++)
                {
                    if (Math.Abs(newA[e, i]) > 1e-8)
                    {
                        j = i;
                        break;
                    }
                }

                if (j == -1)
                    throw new Exception("Cannot find non-zero coefficient in artificial variable row.");

                // Знаходимо вектор, що допоможе вивести штучну змінну з базису
                double[] d = new double[n + m];
                d[j] = 1;
                for (int i = 0; i < m; i++)
                {
                    if (i == e) continue;
                    double k = -newA[i, j] / newA[e, j];
                    for (int l = 0; l < n + m; l++)
                    {
                        newA[i, l] += k * newA[e, l];
                    }
                    y[i] += k * y[e];
                }
                B0[e] = j;

                // Розв'язуємо задачу оптимізації за допомогою симплекс-методу
                simplexResult = new Simplex(c, A, b).maximize();
                x0 = simplexResult.Item2;
                v = simplexResult.Item1;
            }

            // Видаляємо штучні змінні з базису
            int[] finalBasis = new int[m];
            int cnt = 0;
            for (int i = 0; i < m; i++)
                if (B0[i] < n)
                    finalBasis[cnt++] = B0[i];

            // Розв'язуємо задачу оптимізації за допомогою симплекс-методу
            //simplexResult = Simplex(A, b, c, finalBasis); //original
            simplexResult = new Simplex(c, A, b).maximize();

            double[] x = simplexResult.Item2;
            double vFinal = simplexResult.Item1;

            // Перевіряємо коректність розв'язку
            for (int i = 0; i < m; i++)
            {
                double lhs = 0;

                for (int j = 0; j < n; j++)
                    lhs += A[i, j] * x[j];

                if (lhs > b[i] + 1e-8)
                    throw new Exception("The final solution is incorrect.");
            }

            return x;
        }

        public Tuple<double, double[]> Solve(TrainStation TrainStation, Tuple<double, double[]> simplexData)
        {
            int passengersTransported = (int)(simplexData.Item1);

            double t1 = simplexData.Item2[0];
            fastTrains = (int)Math.Truncate(t1);

            double t2 = simplexData.Item2[1];
            passTrains = (int)Math.Truncate(t2);

            trainStation = GetUnusedCars(TrainStation, fastTrains, passTrains);

            while (trainStation.CanAssembleExpressTrain() || trainStation.CanAssemblePassengerTrain())
            {
                // Express train
                if (trainStation.IsExpressTrainBetter())
                {
                    if (!AssembeExpress())
                        AssembePassenger();
                } // Passenger train
                else
                {
                    if (!AssembePassenger())
                        AssembeExpress();
                }
            }

            int totalPassengers = trainStation.PassengersInExpressTrain() * fastTrains + trainStation.PassengersInPassengerTrain() * passTrains;
            return Tuple.Create<double, double[]>(totalPassengers, new double[] { fastTrains, passTrains });
        }

        bool AssembeExpress()
        {
            if (trainStation.CanAssembleExpressTrain())
            {
                trainStation.AssembleExpressTrain();
                fastTrains++;
                return true;
            }
            return false;
        }

        bool AssembePassenger()
        {
            if (trainStation.CanAssemblePassengerTrain())
            {
                trainStation.AssemblePassengerTrain();
                passTrains++;
                return true;
            }
            return false;
        }

        public TrainStation GetUnusedCars(TrainStation data, int fastTrains, int passTrains)
        {
            var returnData = new TrainStation(data);

            returnData.LuggageCar.TotalCount -= data.LuggageCar.ExpressTrainAmount * fastTrains;
            returnData.LuggageCar.TotalCount -= data.LuggageCar.PassengerTrainAmount * passTrains;

            returnData.PostCar.TotalCount -= data.PostCar.ExpressTrainAmount * fastTrains;
            returnData.PostCar.TotalCount -= data.PostCar.PassengerTrainAmount * passTrains;

            returnData.FreightCar.TotalCount -= data.FreightCar.ExpressTrainAmount * fastTrains;
            returnData.FreightCar.TotalCount -= data.FreightCar.PassengerTrainAmount * passTrains;

            returnData.CompartmentCar.TotalCount -= data.CompartmentCar.ExpressTrainAmount * fastTrains;
            returnData.CompartmentCar.TotalCount -= data.CompartmentCar.PassengerTrainAmount * passTrains;

            returnData.SoftSeatedCar.TotalCount -= data.SoftSeatedCar.ExpressTrainAmount * fastTrains;
            returnData.SoftSeatedCar.TotalCount -= data.SoftSeatedCar.PassengerTrainAmount * passTrains;

            return returnData;
        }
    }
}
