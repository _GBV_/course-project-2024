﻿using System;
using System.Collections.Generic;

namespace CourseProject.Methods
{
    public class Simplex
    {
        private double[] c;
        private double[,] A;
        private double[] b;
        private HashSet<int> N = new HashSet<int>();
        private HashSet<int> B = new HashSet<int>();
        private double v = 0;

        public Simplex(double[] c, double[,] A, double[] b)
        {
            int vars = c.Length, constraints = b.Length;

            if (vars != A.GetLength(1))
                throw new Exception("Кількість змінних у c не відповідає числу в A.");

            if (constraints != A.GetLength(0))
                throw new Exception("Кількість обмежень у A не відповідає числу в b.");

            // Розширити максимальний вектор коефіцієнтів fn із заповненням 0
            this.c = new double[vars + constraints];
            Array.Copy(c, this.c, vars);

            // Розширити матрицю коефіцієнтів із заповненням 0
            this.A = new double[vars + constraints, vars + constraints];
            for (int i = 0; i < constraints; i++)
                for (int j = 0; j < vars; j++)
                    this.A[i + vars, j] = A[i, j];

            // Extend constraint right-hand side vector with 0 padding
            // Правий вектор обмеження розширення з доповненням 0
            this.b = new double[vars + constraints];
            Array.Copy(b, 0, this.b, vars, constraints);

            // Populate non-basic and basic sets
            // Заповнити небазисні та базисні набори
            for (int i = 0; i < vars; i++)
                N.Add(i);

            for (int i = 0; i < constraints; i++)
                B.Add(vars + i);
        }

        /// <summary>
        /// повертає Tuple, який містить максимальне значення функції та масив змінних, 
        /// що відповідають оптимальному розв'язку.
        /// </summary>
        /// <returns></returns>
        public Tuple<double, double[]> maximize()
        {
            while (true)
            {
                // Find highest coefficient for entering var
                // Знайти найвищий коефіцієнт для введення змінної
                int e = -1;
                double ce = 0;
                foreach (var _e in N)
                {
                    if (c[_e] > ce)
                    {
                        ce = c[_e];
                        e = _e;
                    }
                }

                // Якщо жоден коефіцієнт не > 0, більше не потрібно максимізувати
                if (e == -1) break;

                // Знайдіть найнижчий коефіцієнт перевірки
                double minRatio = double.PositiveInfinity;
                int l = -1;
                foreach (var i in B)
                {
                    if (A[i, e] > 0)
                    {
                        double r = b[i] / A[i, e];
                        if (r < minRatio)
                        {
                            minRatio = r;
                            l = i;
                        }
                    }
                }

                // Unbounded
                if (double.IsInfinity(minRatio))
                    return Tuple.Create<double, double[]>(double.PositiveInfinity, null);

                pivot(e, l);
            }

            // Витягніть суми та змінні для оптимального рішення
            double[] x = new double[b.Length];
            int n = b.Length;
            for (var i = 0; i < n; i++)
                x[i] = B.Contains(i) ? b[i] : 0;

            // Повернути максимум і змінні
            return Tuple.Create<double, double[]>(v, x);
        }

        /// <summary>
        /// Метод pivot виконує перехід до нового базису шляхом обміну змінних в
        /// базисі та небазисі, а також обчислення нових коефіцієнтів.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="l"></param>
        private void pivot(int e, int l)
        {
            N.Remove(e);
            B.Remove(l);

            b[e] = b[l] / A[l, e];

            foreach (var j in N)
                A[e, j] = A[l, j] / A[l, e];

            A[e, l] = 1 / A[l, e];

            foreach (var i in B)
            {
                b[i] = b[i] - A[i, e] * b[e];

                foreach (var j in N)
                    A[i, j] = A[i, j] - A[i, e] * A[e, j];

                A[i, l] = -1 * A[i, e] * A[e, l];
            }

            v = v + c[e] * b[e];

            foreach (var j in N)
                c[j] = c[j] - c[e] * A[e, j];

            c[l] = -1 * c[e] * A[e, l];

            N.Add(l);
            B.Add(e);
        }
    }
}
