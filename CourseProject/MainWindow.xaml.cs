﻿using CourseProject.Methods;
using CourseProject.Models;
using System.Windows;

namespace CourseProject
{
    public partial class MainWindow : Window
    {
        protected TrainStation trainStation { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            SetWarningAlert(false);
        }

        protected void InitializeTrainCars()
        {
            trainStation = new TrainStation()
            {
                LuggageCar = TrainCar.Parse(
                    LuggageCarTotalCountLabel.Text,
                    LuggageCarExpressTrainAmountLabel.Text,
                    LuggageCarPassengerTrainAmountLabel.Text,
                    LuggageCarPassengerCountLabel.Text),

                PostCar = TrainCar.Parse(
                    PostCarTotalCountLabel.Text,
                    PostCarExpressTrainAmountLabel.Text,
                    PostCarPassengerTrainAmountLabel.Text,
                    PostCarPassengerCountLabel.Text),

                FreightCar = TrainCar.Parse(
                    FreightCarTotalCountLabel.Text,
                    FreightCarExpressTrainAmountLabel.Text,
                    FreightCarPassengerTrainAmountLabel.Text,
                    FreightCarPassengerCountLabel.Text),

                CompartmentCar = TrainCar.Parse(
                    CompartmentCarTotalCountLabel.Text,
                    CompartmentCarExpressTrainAmountLabel.Text,
                    CompartmentCarPassengerTrainAmountLabel.Text,
                    CompartmentCarPassengerCountLabel.Text),

                SoftSeatedCar = TrainCar.Parse(
                    SoftSeatedCarTotalCountLabel.Text,
                    SoftSeatedCarExpressTrainAmountLabel.Text,
                    SoftSeatedCarPassengerTrainAmountLabel.Text,
                    SoftSeatedCarPassengerCountLabel.Text),
            };
        }

        protected void CalculateButton_Click(object sender, RoutedEventArgs e)
        {
            InitializeTrainCars();

            if (trainStation.PassengerCountSum() == 0)
            {
                SetWarningAlert(true);
            }
            else
            {
                SetWarningAlert(false);
                Gomori();
            }
        }

        protected void SetWarningAlert(bool state)
        {
            if (state)
            {
                labelDanger.Visibility = Visibility.Visible;
                LabelTrainsAnswer.Visibility = Visibility.Hidden;
                LabelPassengersAnswer.Visibility = Visibility.Hidden;
            }
            else
            {
                labelDanger.Visibility = Visibility.Hidden;
                LabelTrainsAnswer.Visibility = Visibility.Visible;
                LabelPassengersAnswer.Visibility = Visibility.Visible;
            }
        }

        protected void Gomori()
        {
            double[] totalNumberOfTransported = new double[]
            {
                (trainStation.LuggageCar.TotalPassengersInExpressTrain()
                + trainStation.PostCar.TotalPassengersInExpressTrain()
                + trainStation.FreightCar.TotalPassengersInExpressTrain()
                + trainStation.CompartmentCar.TotalPassengersInExpressTrain()
                + trainStation.SoftSeatedCar.TotalPassengersInExpressTrain()),

                (trainStation.LuggageCar.TotalPassengersInPassengerTrain()
                + trainStation.PostCar.TotalPassengersInPassengerTrain()
                + trainStation.FreightCar.TotalPassengersInPassengerTrain()
                + trainStation.CompartmentCar.TotalPassengersInPassengerTrain()
                + trainStation.SoftSeatedCar.TotalPassengersInPassengerTrain())
            };

            double[,] variablesLeftPart = new double[,]
            {
                { trainStation.LuggageCar.ExpressTrainAmount, trainStation.LuggageCar.PassengerTrainAmount },
                { trainStation.PostCar.ExpressTrainAmount, trainStation.PostCar.PassengerTrainAmount },
                { trainStation.FreightCar.ExpressTrainAmount, trainStation.FreightCar.PassengerTrainAmount },
                { trainStation.CompartmentCar.ExpressTrainAmount, trainStation.CompartmentCar.PassengerTrainAmount },
                { trainStation.SoftSeatedCar.ExpressTrainAmount, trainStation.SoftSeatedCar.PassengerTrainAmount },
            };

            double[] variablesRightPart = new double[]
            {
                trainStation.LuggageCar.TotalCount,
                trainStation.PostCar.TotalCount,
                trainStation.FreightCar.TotalCount,
                trainStation.CompartmentCar.TotalCount,
                trainStation.SoftSeatedCar.TotalCount,
            };

            var simplexExpr = new Simplex(totalNumberOfTransported, variablesLeftPart, variablesRightPart);
            var answerSimplex = simplexExpr.maximize();
            var answerGomori = new Gomori().Solve(trainStation, answerSimplex);

            LabelTrainsAnswer.Content = $"Оптимальна кількість поїздів:\n{answerGomori.Item2[0]} швидких та {answerGomori.Item2[1]} пасажирських";
            LabelPassengersAnswer.Content = $"Кількість перевезених пасажирів: {answerGomori.Item1}";
        }
    }
}
