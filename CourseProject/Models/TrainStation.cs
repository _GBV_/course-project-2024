﻿namespace CourseProject.Models
{
    public class TrainStation
    {
        /// <summary>
        /// Багажний вагон
        /// </summary>
        public TrainCar LuggageCar;

        /// <summary>
        /// Поштовий вагон
        /// </summary>
        public TrainCar PostCar;

        /// <summary>
        /// Жорсткий вагон
        /// </summary>
        public TrainCar FreightCar;

        /// <summary>
        /// Купейний вагон
        /// </summary>
        public TrainCar CompartmentCar;

        /// <summary>
        /// М’який вагон
        /// </summary>
        public TrainCar SoftSeatedCar;

        public TrainStation() { }

        public TrainStation(TrainStation station)
        {
            LuggageCar = station.LuggageCar;
            PostCar = station.PostCar;
            FreightCar = station.FreightCar;
            CompartmentCar = station.CompartmentCar;
            SoftSeatedCar = station.SoftSeatedCar;
        }

        public int PassengerCountSum()
        {
            return LuggageCar.PassengerCount
                + PostCar.PassengerCount
                + CompartmentCar.PassengerCount
                + FreightCar.PassengerCount
                + SoftSeatedCar.PassengerCount;
        }

        public bool CanAssembleExpressTrain()
        {
            return LuggageCar.TotalCount >= LuggageCar.ExpressTrainAmount
                && PostCar.TotalCount >= PostCar.ExpressTrainAmount
                && FreightCar.TotalCount >= FreightCar.ExpressTrainAmount
                && CompartmentCar.TotalCount >= CompartmentCar.ExpressTrainAmount
                && SoftSeatedCar.TotalCount >= SoftSeatedCar.ExpressTrainAmount;
        }

        public bool CanAssemblePassengerTrain()
        {
            return LuggageCar.TotalCount >= LuggageCar.PassengerTrainAmount
                && PostCar.TotalCount >= PostCar.PassengerTrainAmount
                && FreightCar.TotalCount >= FreightCar.PassengerTrainAmount
                && CompartmentCar.TotalCount >= CompartmentCar.PassengerTrainAmount
                && SoftSeatedCar.TotalCount >= SoftSeatedCar.PassengerTrainAmount;
        }

        public int PassengersInExpressTrain()
        {
            return LuggageCar.TotalPassengersInExpressTrain()
                + PostCar.TotalPassengersInExpressTrain()
                + FreightCar.TotalPassengersInExpressTrain()
                + CompartmentCar.TotalPassengersInExpressTrain()
                + SoftSeatedCar.TotalPassengersInExpressTrain();
        }

        public int PassengersInPassengerTrain()
        {
            return LuggageCar.TotalPassengersInPassengerTrain()
                + PostCar.TotalPassengersInPassengerTrain()
                + FreightCar.TotalPassengersInPassengerTrain()
                + CompartmentCar.TotalPassengersInPassengerTrain()
                + SoftSeatedCar.TotalPassengersInPassengerTrain();
        }

        public void AssembleExpressTrain()
        {
            LuggageCar.TotalCount -= LuggageCar.ExpressTrainAmount;
            PostCar.TotalCount -= PostCar.ExpressTrainAmount;
            FreightCar.TotalCount -= FreightCar.ExpressTrainAmount;
            CompartmentCar.TotalCount -= CompartmentCar.ExpressTrainAmount;
            SoftSeatedCar.TotalCount -= SoftSeatedCar.ExpressTrainAmount;
        }

        public void AssemblePassengerTrain()
        {
            LuggageCar.TotalCount -= LuggageCar.PassengerTrainAmount;
            PostCar.TotalCount -= PostCar.PassengerTrainAmount;
            FreightCar.TotalCount -= FreightCar.PassengerTrainAmount;
            CompartmentCar.TotalCount -= CompartmentCar.PassengerTrainAmount;
            SoftSeatedCar.TotalCount -= SoftSeatedCar.PassengerTrainAmount;
        }

        public bool IsExpressTrainBetter()
            => PassengersInExpressTrain() > PassengersInPassengerTrain();
    }
}
