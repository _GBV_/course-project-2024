﻿namespace CourseProject.Models
{
    public class TrainCar
    {
        public int TotalCount { get; set; }
        public int ExpressTrainAmount { get; set; }
        public int PassengerTrainAmount { get; set; }
        public int PassengerCount { get; set; }

        public TrainCar(int totalCount, int expressTrainAmount, int passengerTrainAmount, int passengers)
        {
            TotalCount = totalCount;
            ExpressTrainAmount = expressTrainAmount;
            PassengerTrainAmount = passengerTrainAmount;
            PassengerCount = passengers;
        }

        public static TrainCar Parse(string totalCount, string expressTrainAmount, string passengerTrainAmount, string passengerCount)
            => new TrainCar(int.Parse(totalCount), int.Parse(expressTrainAmount), int.Parse(passengerTrainAmount), int.Parse(passengerCount));

        public int TotalPassengersInExpressTrain()
            => PassengerCount * ExpressTrainAmount;

        public int TotalPassengersInPassengerTrain()
            => PassengerCount * PassengerTrainAmount;
    }
}
